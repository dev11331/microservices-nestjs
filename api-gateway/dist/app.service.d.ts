import { ClientProxy } from '@nestjs/microservices';
export declare class AppService {
    private readonly clientOrderService;
    private readonly clientPaymentService;
    constructor(clientOrderService: ClientProxy, clientPaymentService: ClientProxy);
    createOrder(data: any): void;
    payment(data: any): void;
    pingOrderService(): import("rxjs").Observable<{
        message: string;
        duration: number;
    }>;
    pingPaymentService(): import("rxjs").Observable<{
        message: string;
        duration: number;
    }>;
}
