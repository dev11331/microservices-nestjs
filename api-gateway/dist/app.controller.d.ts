import { AppService } from './app.service';
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    pingOrderService(): import("rxjs").Observable<{
        message: string;
        duration: number;
    }>;
    pingPaymentService(): import("rxjs").Observable<{
        message: string;
        duration: number;
    }>;
    createOrder(data: any): void;
    payment(data: any): void;
}
