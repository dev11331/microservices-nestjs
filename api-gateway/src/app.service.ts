import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { map } from 'rxjs/operators';

@Injectable()
export class AppService {
  constructor(
    @Inject('ORDER_SERVICE') private readonly clientOrderService: ClientProxy,
    @Inject('PAYMENT_SERVICE') private readonly clientPaymentService: ClientProxy,
  ) {}

  createOrder(data)
  {
    this.clientOrderService.emit('createorder', data);
  }

  payment(data){
    this.clientPaymentService.emit('checkpayment', data);
  }
  //TODO: check purpose
  pingOrderService() {
    const startTs = Date.now();
    const pattern = { cmd: 'ping' };
    const payload = {};
    return this.clientOrderService
      .send<string>(pattern, payload)
      .pipe(
        map((message: string) => ({ message, duration: Date.now() - startTs })),
      );
  }

  //TODO: check purpose
  pingPaymentService() {
    const startTs = Date.now();
    const pattern = { cmd: 'ping' };
    const payload = {};
    return this.clientPaymentService
      .send<string>(pattern, payload)
      .pipe(
        map((message: string) => ({ message, duration: Date.now() - startTs })),
      );
  }
}
