import { Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get('/ping-order')
  pingOrderService() {
    return this.appService.pingOrderService();
  }

  @Get('/ping-payment')
  pingPaymentService() {
    return this.appService.pingPaymentService();
  }

  @Post('/create-order')
  createOrder(data) {
    console.log(data);
    return this.appService.createOrder(data)
  }

  @Post('/payment')
  payment(data) {
    console.log(data);
    return this.appService.payment(data)
  }

}
