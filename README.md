This solution demo how to build and call Microservices with nest.js
- orders: microservices create an order.
- payments: receive order and random status of payment, if fail set status code os order to 0, set to 3 if successful
- api-gateway: point to two mricroservices to call create order and checkpayment

Build anh run the code:
1. Install postgres, create user order_sp, set pass word order123 with supperuser role.
2. Create database orders, owner by order_sp
3. Install RabbitMQ Server to create microservice
4. Install Node.js, Nest.js, type script
5. Run payments service: nmp run listen
6. Run orders: nmp run listen
7. Run api-gateway: npm run start or npm run start:dev
7. use postman create POST request
-  To create order
    url: http://localhost:3001/create-order
    body raw/JSON
    ```json
    {
        "customerId":1,
        "productId":1,
        "statusCode": 0,
        "orderDate": "2021-11-18",
        "amount":100
    }
    ```
- To payment
    url: http://localhost:3001/payment
    body raw/JSON
    ```json
    {
        "orderId":1,
        "customerId":1,
        "productId":1,
        "statusCode": 0,
        "orderDate": "2021-11-18",
        "amount":100
    }
    ```
send request, payment status display in console
TODO: improve payments to return data to orders, add unit test
