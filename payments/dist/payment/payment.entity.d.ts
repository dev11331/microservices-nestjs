export declare class Payment {
    orderId: number;
    amount: number;
    paymentStatus: boolean;
}
