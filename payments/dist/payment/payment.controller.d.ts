import { PaymentService } from './payment.service';
export declare class PaymentController {
    private paymentService;
    constructor(paymentService: PaymentService);
    payment(data: any): Promise<any>;
    checkpayment(data: any): Promise<void>;
}
