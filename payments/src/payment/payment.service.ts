import { Injectable } from '@nestjs/common';

@Injectable()
export class PaymentService {
    async paymentStatus(data): Promise<any>{
        let status = Math.random() < 0.5;
        if (status) {
            data.statusCode = 3;
            console.log('payment surcessful. Code: ' + data.statusCode);
        } else{
            data.statusCode = 0;
            console.log('payment fail. Code: ' + data.statusCode);
        }
        
        return (data); 
    }
}
