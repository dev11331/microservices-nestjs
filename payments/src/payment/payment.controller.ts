import { Controller, Body, Post } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { PaymentService } from './payment.service';

@Controller('paymentstatus')
export class PaymentController {
    constructor(private paymentService: PaymentService) {
    }
    @Post()
    payment(data) {
        return this.paymentService.paymentStatus(data);
    }
    @EventPattern('checkpayment')
    async checkpayment(data) {
        await this.paymentService.paymentStatus(data);
    }
}
