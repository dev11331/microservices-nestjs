export class Order {
    id: number;
    customerId: number;
    productId: number;
    statusCode: number;
    orderDate: Date;

    amount: number;
};