import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { OrderModule } from './order/order.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      "type": "postgres",
      "host": "localhost",
      "port": 5432,
      "username": "order_sp",
      "password": "order123",
      "database": "orders",
      "autoLoadEntities": true,
      "synchronize": true
    }),
    OrderModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
