import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'This is sample code to CRUD product order and call payment services to check payment status';
  }
}
