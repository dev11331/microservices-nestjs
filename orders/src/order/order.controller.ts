import { Body, Controller, Delete, Get, Inject, Param, Post, Put } from '@nestjs/common';
import { ClientProxy, EventPattern } from '@nestjs/microservices';
import { OrderService } from './order.service';

@Controller('orders')
export class OrderController {
    constructor(private orderService: OrderService){
    }

    @Get()
    async all() {
        return this.orderService.all();
    }

    @Post()
    async create(
        @Body('customerId') customerId: number,
        @Body('productId') productId: number,
        @Body('statusCode') statusCode: number,
        @Body('orderDate') orderDate: Date,
        @Body('amount') amount: number,
    ) {
        const order = await this.orderService.create({
            customerId,
            productId,
            statusCode,
            orderDate,
            amount
        });

        return order;
    }

    @Get(':id')
    async get(@Param('id') id: number){
        return this.orderService.get(id);
    }

    @Put(':id')
    async update(
        @Param('id') id: number,
        @Body('customerId') customerId: number,
        @Body('productId') productId: number,
        @Body('statusCode') statusCode: number,
        @Body('orderDate') orderDate: Date,
        @Body('amount') amount: number,
    ){
        return this.orderService.update(id, {
            customerId,
            productId,
            statusCode,
            orderDate,
            amount
        })
    }

    @Delete(':id')
    async delete(@Param('id') id: number){
        return this.orderService.delete(id)
    }
    
    @EventPattern('createorder')
    async checkpayment(data) {
        await this.orderService.create({data});
    }
}
