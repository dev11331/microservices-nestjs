import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Order } from './order.entity';

@Injectable()
export class OrderService {
    constructor(
        @InjectRepository(Order) private readonly orderRepository: Repository<Order>
    ) { }

    async all(): Promise<Order[]> {
        return this.orderRepository.find();
    }

    async create(data): Promise<Order> {
        return this.orderRepository.save(data);
    }

    async get(id:number): Promise<Order> {
        return this.orderRepository.findOne({id});
    }

    async update(id:number, data): Promise<any>{
        return this.orderRepository.update(id, data);
    }

    async delete(id:number): Promise<any>{
        return this.orderRepository.delete(id);
    }
}
