import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Order {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    customerId: number;
    @Column()
    productId: number;
    @Column()
    statusCode: number;
    @Column()
    orderDate: Date;
    @Column()
    amount: number;
};