import { OrderService } from './order.service';
export declare class OrderController {
    private orderService;
    constructor(orderService: OrderService);
    all(): Promise<import("./order.entity").Order[]>;
    create(customerId: number, productId: number, statusCode: number, orderDate: Date, amount: number): Promise<import("./order.entity").Order>;
    get(id: number): Promise<import("./order.entity").Order>;
    update(id: number, customerId: number, productId: number, statusCode: number, orderDate: Date, amount: number): Promise<any>;
    delete(id: number): Promise<any>;
    checkpayment(data: any): Promise<void>;
}
