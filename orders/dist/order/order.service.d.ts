import { Repository } from 'typeorm';
import { Order } from './order.entity';
export declare class OrderService {
    private readonly orderRepository;
    constructor(orderRepository: Repository<Order>);
    all(): Promise<Order[]>;
    create(data: any): Promise<Order>;
    get(id: number): Promise<Order>;
    update(id: number, data: any): Promise<any>;
    delete(id: number): Promise<any>;
}
